# Installation

1 - Start Jupyter by running the ‘jupyter notebook’ command in the command line prompt/terminal
- If necessary, install Jupyter using Anaconda or the PIP package manager
    - To install using Anaconda, first go to the Anaconda website https://www.anaconda.com/products/individual and follow the instructions to install Anaconda for your respective operating system
    - To Install using PIP, run ‘pip install notebook’


2 - Go to Team Argonne’s Repository: https://gitlab.msu.edu/alyaqou1/argonne-capstone


3 - Download the repository as a .zip folder by clicking on the down arrow next to the blue ‘clone’ button


4 - Unzip the project repository into a folder on your computer


5 - Open a command line prompt/terminal and navigate to the unzipped folder using the ‘cd’ command


6 - Create a Conda environment by running ‘conda env create -f environment.yml’


7 - To install the following packages for each individual example file:
- Install Matrixprofile-ts: Run ‘pip install matrixprofile-ts’ in your terminal, or in a Jupyter terminal
- Install Stumpy: Run '!pip install stumpy' in your terminal, or in a Jupyter terminal
- Install Sklearn: Run 'pip install -U scikit-learn' in your terminal, or in a Jupyter terminal
- Install Tensorflow/Keras: Run 'pip install tensorflow' in your terminal, or in a Jupyter terminal
  - Note: Tensorflow requires the latest version of pip and Python 3.7 or higher
  - To update pip: Run '!pip install --upgrade pip'
- Install PyCaret: Run '!pip install pycaret' in your terminal, or in a Jupyter terminal
- If necessary, also install numpy, matplotlib, and pandas.

8 - From jupyter navigate to the Examples folder to:
 - Run the example tutorial 'early_tutorial.ipynb' and see if the required packages (matrixprofile-ts) were successfully installed using pip
 - Run the '1-dimensional-analysis.ipynb' to see if the required stumpy and matrixprofile packages were successfully installed using pip
 - Run the 'SVM_Test__2_.ipynb' to see if the required sklearn package was successfully installed using pip
 - Run the 'Time-series-LSTM.ipynb' to see if the required tensorflow and keras packages were successfully installed using pip
 - Run the 'KNN_Anomaly_Detection.ipynb' to see if the required PyCaret package was successfully installed using pip


