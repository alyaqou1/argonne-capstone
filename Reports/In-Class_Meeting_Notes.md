## In Class Meeting Notes [Brainstorm Session]

### Date: Feb 16, 2022 
### Time: 3:30pm-4:00pm

#### Attendance:
- Emani Hunter
    - Role: Notetaker
- Will Diamond
    - Role: Timekeeper
- Mohammed Alyaqoub
    - Role: Facilitator
- Ndeye Gueye
    - Role: Notetaker
    
#### Agenda:
- Brainstorm Ideas for future capstone projects related to the current project

#### Discussion:
- Ideas:
    - Create specified anomaly detection package
    - Detect anomalies for categorical/qualitative data instead of numerical data
    - Create ML algorithm(s) to predict anomalies in a future forecasted time interval
    - Incorporate trends/ seasonality
    - Image recognition (can incorporate the ideas of detecting anomalous points/pixels in an image)
    - Expanding anomaly detection to other languages/packages (i.e., R)

