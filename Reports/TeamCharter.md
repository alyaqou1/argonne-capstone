# Argonne National Lab Team Charter

### Members/Background/Roles
- Emani Hunter
    - Background in Computer Science, Data Science and CMSE
    - Role for first scheduled meeting: Facilitator
- Mohammed Alyaqoub
    - Background in Computer Science, Data Science and CMSE
    - Role for first scheduled meeting: Note Taker
- Ndeye Gueye 
    - Background in Data Science and CMSE
    - Role for first scheduled meeting: Time Keeper
- Will Diamond
    - Background in Data Science and Computer Science
    - Role for first scheduled meeting: 
    



### Team Objectives - Purpose and Goals
The main goal of the Argonne Team is to research, test, and compare software and examples to detect anomalies in time series data.

- Two most important questions to answer:

    - What solutions are available for time series anomaly detection?
    - How well do the solutions found work?


### Team Scope - What do we want to get done?
Following the 4-step pipeline:

- Collection/Generation:
    - Benchmark example datasets found online 
    - Simulated or real-time data created by team
    - Example datasets provided by the sponsors 

- Cleaning/munging:
    - Team must take into account preprocessing steps to identify and clean the most common source of data  

- Modeling building/analysis:
    - Team will research and test different time-series anomaly detection algorithms

- Visualization/storytelling:
    - Team must demonstrate the feasibility of using the algorithms for this application, which includes reporting where the approaches work best and when they are likely to fail 

At the end of this project, we hope to deliver interactive results that can be used to motivate the development of downstream applications for extreme event/anomaly detection.


### Team Strategy
- Collaboration: working together as a team to accomplish our goal, provide help if a team member is in need of it (also a ground rule).
- Work alongside the sponsor to continuously improve results
    - Contact/meet with sponsor consistently and in a professional manner


### Plans/Metrics/Ground Rules
- Time: Respect for time
    - Be on time to each meeting
- Deadlines:
    - Notify team members of any unexpected delays as soon as possible so the issue can be resolved quickly
- Method of Communication: Slack channel
    - #argonne-team
- Plans: Git Software to use: GitLab
- Plans: Programming language to use: Python
- Ground rule: Help each other even if the tasks have been divided up
    - Be inclusive (and work together)
- Ground rule: Meeting minutes notes should be succinct and easy to read for each group member
- Ground rule: Notify team members if you are unable to attend a meeting
- Ground rule: Make sure tasks are distributed evenly so as not to cause distress on other team members
- Ground rule: Notify team before pushing to repository to avoid any conflicts in the files


### Membership/Meetings/Reports
- Weekly Meeting Time: M, W, F after 4 pm, Sundays
    - On zoom (if in-person not sufficient/preferred)
- Action items at the end of each meeting should be well defined and assigned to group members


### Signatures

- Mohammed Alyaqoub, Emani Hunter, Ndeye Gueye, Will Diamond


