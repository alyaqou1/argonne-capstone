 

# Anomaly Detection in Time Series Data

Sponsor: Argonne National Laboratory

By: Mohammed Alyaqoub, Will Diamond, Ndeye Gueye, Emani Hunter

![Argonne National Laboratory](anl_logo_head.png)

### Overview

The Argonne Team intends to complete a comprehensive study on the feasibility, speed, and accuracy of different anomaly detection algorithms for time series data using variable dimension and sized data sets. The Argonne Team’s goal is to answer two overarching questions, which later on will be broken down into smaller parts. Such questions consist of figuring out what solutions are available for time series anomaly detection and testing to see how well these solutions work. 

In order to answer these questions, the team will follow a data analytical pipeline. The team will first collect and generate provided data, clean and munge the data, build time-series models, and visualize these models for further analysis. The end deliverable of this project should showcase interactive and reproducible results that can be used to motivate the development of downstream applications for extreme event/anomaly detection. 

### Program Description

The project can be broken down into separate parts each with different challenges. The first part of the project will constitute the collection of data-sets of time series data with anomalies and different anomaly detection algorithms. Through google scholar and github we can collect these algorithms to use for testing and the datasets we will be testing them on. Our Argonne contact has also offered to provide a higher dimensional real world data set when we have sufficiently started testing multiple of our own researched algorithms. 

The next step is to compile all different algorithms into a single python file. We are considering using group programming software to code simultaneously or coordinate through github commits. This python file will be the master testing file, which can be applied to any different standardized data set. 

Even further, we can include code to grade and time the performance of each algorithm or group of algorithms. Using matplotlib and other graphing python libraries we can clearly present our findings on the effectiveness of different algorithms on different dimensionalities. Algorithms from the matrixprofile-ts github were provided in the project description, but we really want to look for a diverse group of anomalous detection algorithms to test. 


### Project Goals / Timeline / Labor

The goal of this project is to create a program that can identify anomalies within multi-dimensional data. We plan on doing so through the following steps:

- Research literature to get a good idea of where to start
    - Do a literature review on time-series anomaly algorithms
    - Find benchmark data to use to apply already existing algorithms
- Start with determining anomalies in one-dimensional (benchmark) datasets
    - Apply Sponsor suggestion: matrixprofile-ts to benchmark datasets
    - Create computational models to visualize the time series patterns of these datasets
- Use the same method on multi-dimensional (high-dimensional) datasets and compare instances of anomalies within each variable/feature. Instances flagged at multiple dimensions will be classified as anomalies.
- Obtain high-dimensional data from Sponsor
    - Pre-process, clean and munge data for use
- Test already existing time-series algorithms on pre-processed/ standardized version of high-dimensional data provided by the Sponsor
    - Work closely with Sponsor to improve such algorithms to allow for accurate anomaly detection
- Create an interactive and robust jupyter notebook to reproduce algorithm experiments and any necessary computational models


### Anticipating Challenges

Finding benchmark data sets and real world higher-dimensional data sets will be a challenge. This will be the biggest challenge of our research and literature review portion of the project. We are really going to have to do some digging to get a diverse group of detection algorithms and a diverse group of datasets. Part of this challenge is finding a way to standardize the time series data so we can automatically test a slew of anomaly detection algorithms. We do not think we will have to do much manual cleaning of the data, and hope most can be done programmatically. 

When we move onto testing real world datasets the dimensionality and size of the datasets may increase drastically. This provides some new challenges on testing the effectiveness of the algorithms. There is a possibility that many of these anomaly detection algorithms we find will be incredibly computationally inefficient. For some of our real world datasets it could take a lot of computing power and time to test and detect these anomalies. Part of the project is testing the feasibility of using different algorithms on this real world data. It is very possible that through the course of our research and development that we find it is too difficult to automatically detect anomalies on hugely dimensional datasets, taking too long or too many resources. 

Datasets provided by Sponsor may be ‘encoded’ and not easily readable. So, data preprocessing tools will need to be used to decode the data for readability and usability purposes. This may take quite some time and can give rise to a back-up/ obstacle for the team.


 
