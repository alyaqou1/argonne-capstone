## Argonne Weekly Meeting Notes

#### Date:Apr 13, 2022
#### Time: 4pm-5pm

_ **Note: Meeting with Sponsor (Romit Maulik)** _

**Attendance:**

- Emani Hunter
- Will Diamond
- Mohammed Alyaqoub
- Ndeye Gueye

**Agenda:**

- Discuss clarifying questions
- Understand "w" from given data

**Discussion:**

- What is the value 'w'- vorticity
  - Measure of the rotation that is happening
  - Raw image plotted
  - Interesting: at instance compute sum of vorticity squared and add it all up and average it, as you measure the quantity over time you will see spikes (burst conditions)
  - Sum of each frame squared
  - Physical significance: can approx fluids in the atmosphere to be 2D
- Problem: look at half the sum of vorticity squared
  - Look at just the velocity on the 32x32 grid
  - Detection pipeline- learn to sum up square of each (sum of squares divided by 2)
- Half sum of w^2 and setting threshold (supervised part)
- Put supervised vs. unsupervised in report
- Connection between KE
  - Instrophy
  - Analog of how much rotational energy is there
- Does not suggest to move on to a new dataset

#### Date:Apr 6, 2022
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Will Diamond
- Mohammed Alyaqoub

**Agenda:**

- Discuss and complete 3x3 reflections
- Discuss algorithm progress made so far
- Draft questions to ask sponsor

**Discussion:**

- Will - Initial testing of SVM
  - Thinking about finding a different algorithm for unsupervised learning
- Emani- Tested LSTM Autoencoders
  - Currently trying to test out DeepAnT algorithm
- Mohammed- Tested STUMPY
  - Got it working similar to matrixprofile-ts

**Action Items / Next steps:**

- Work on Future Project Ideas assignment due Sunday (April 10)
- Finish up algorithm/code testing by Wednesday (April 13)
- Meet with professor/ sponsor to discuss the project thus far and installation instructions
- Start the report!!!

#### Date:Mar 16, 2022
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Ndeye Gueye
- Will Diamond
- Mohammed Alyaqoub

**Agenda:**

- Discuss and complete 3x3 reflections
- Discuss sponsor provided data
- Day/time to record closed loop slides

**Discussion:**

- Create a list of questions to ask the sponsor about the data provided
  - What is the 'w'data?
- As of right now the only reasonable day/time to meet to record the closed loop presentation slides is Sunday
-

**Action Items / Next steps:**

- Record closed loop presentation slides
  - Meeting sunday at 4 PM
- Better understand new 3d sponsor dataset.
  - Come up with list of questions to ask sponsor which we can send next Wednesday.
- Try to have a basic working algorithm generated on the sponsor provided data
  - Discuss and show during the next team meeting (Wednesday @4pm)

#### Date:Mar 11, 2022
#### Time: 12:00pm

_**Note: Meeting with Sponsor (Romit Maulik)**_

**Attendance:**

- Mohammed Alyaqoub
- Emani Hunter
- Ndeye Gueye

**Agenda/Questions:**

- High dimensional datasets
- Project proposal feedback
- Familiarity with Matrixprofile-ts (?)

**Discussion/Suggestions:**

- Use algorithm on (32 r x32 c data) to predict whether we are in an anomalous state and fit it on the norm time-series
  - Help benchmark how the algorithm works
  - Field = original representation , norm= compressed representation
  - Run all algorithms on this dataset
- Argonne only looked at whether the norm is greater than a threshold (visual thresholding) instead of doing data analysis
- Kinetic energy over 60 is the window for the norm data
- Fluid dynamics dataset

#### Date:Mar 3, 2022
#### Time: 3:30pm-4:00pm

_ **Note: Meeting with Dr. Colbry** _

**Attendance:**

- Mohammed Alyaqoub
- Will Diamond
- Ndeye Gueye

**Agenda/Questions:**

- Discuss open loops
-
-

**Discussion/Suggestions:**

- Dirk's complaints
  - Setting up environment file slightly better
    - Will take longer to watch video than fix it
- Initial thoughts about project
- We should start writing our report now
  - Help organize our thoughts to find if we're missing something\
  - Worried we might spend too much time finding new algorithms
- Wants to see section in our report showing a synopsis on where we got each dataset, and a short section on how to download this data into a new notebook
  - Have a notebook called download data describing how to acquire all our datasets we use from different sources (datasets are part of the deliverables, step 1 of the project)
    - Where did it come from?
    - How to download?
    - Some attributes of the dataset
    - All for each dataset
- Another notebook with an explanation of all our algorithms
  - Here's what it does
  - Here's why we picked this
  - Comparison of the tools
  - Conclusion of our algorithms
    - Pros and cons list

- Matrixprofile-ts bug errorDont worry too much about debugging this
-
  - Just explain our process. "We tried it, come up with this error."
    - Leave code in notebook, even if it errors
    - Mention in future works and concluding discussion sections of report
      - Concluding discussion section can include recommendation on which algorithm we would use
    - Spend more time moving on to different parts of the project
    - Dirk might help us with the error if we document it really well lol

**IMPORTANT PART**

- Closing loops
  - Step 1: Come up with datasets
    - Already pretty much done this. Just need to include a table/notebbook in our report that explains where our data comes from
  - Step 2: Explore matrixprofile-ts
  - Step 3: Find more algorithms to test
    - Make a table of each algorithm for our report
      - Has features of the algorithm
      - Some "pro/cons" ish
      - Can include algorithms we didn&#39;t end up testing
      - Can include results in the table too
        - How hard is it to get working?
        - How long does it take to run?
        - How accurate is it?
        - Usability on different data?

#### Date: Mar 2, 2022 
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Mohammed Alyaqoub
- Will Diamond

**Agenda:**

- Go over PCA and matrixprofile discords code
- Discuss and complete 3x3 reflections
- Discuss SB meeting availability
- Questions to ask professor during team/instructor meeting

**Discussion:**

- Work on matrixprofile-ts PCA
- Possbile progression to moving on from matrixprofile-ts to working with new algorithms found
- Overarching questions to ask professor:
  - Advice on how to implement PCA?
  - Python package recommendations
  - Suggestions on how to keep track of all algorithms to use and test
- Autoencoders using pyOD:[https://neptune.ai/blog/anomaly-detection-in-time-series](https://neptune.ai/blog/anomaly-detection-in-time-series)

**Action Items / Next steps:**

- Meeting next Saturday
  - Work on closed loop slides (due Sunday)
- Close open loops and create closed loop presentation slides

#### Date:Feb 23, 2022
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Mohammed Alyaqoub
- Ndeye Gueye

**Agenda:**

- Go over matrixprofile discords code
- Create jupyter notebook/python file to collaborate and work on together
- Discuss 3x3 reflections
- Matrixprofile coding session

**Discussion:**

- Everyone has an understanding of the matrixprofile-ts library/repo provided by the sponsor
  - Currently focusing on the 'discords' examples and source code to visually detect outliers
- Does k have a limit?
  - k in matrixprofile discords()
- Overflow errors for large data: _python int too large to convert c long_

**Action Items / Next steps:**

- Work on improving matrixprofile discords code
- Create synthetic data to work with
  - Data where we create the 'anomalies' and already know how many there are
- Coordinate with Boeing Team
  - They are using time series data as well and would like to collaborate
    - STUMPY
    - Matrxiprofile
    - AutoML

#### Date:Feb 16, 2022
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Will Diamond
- Mohammed Alyaqoub
- Ndeye Gueye

**Agenda:**

- Overview of matrixprofile-ts
  - How did it go?
  - Discussion of tutorial

**Discussion:**

- Ran into issues with running a dataset in matrixprofile-ts jupyter notebook
  - Dataset has been found: [https://raw.githubusercontent.com/numenta/NAB/master/data/realKnownCause/nyc\_taxi.csv](https://raw.githubusercontent.com/numenta/NAB/master/data/realKnownCause/nyc_taxi.csv)
- Each member was able to successfully run Matrix Profile Tutorial Jupyter notebook
- Create one large .py file or jupyter notebook to write code in and keep everyone on the same page
-

**Action Items / Next steps:**

- Find a way to grade the algorithms in matrixprofile-ts on the benchmark data
  - Runtime
  - Accuracy
  - Performance
- Continue getting familiarized with matrixprofile-ts
  - How do we find anomalous data points using matrixprofile-ts?
    - Motif notebook?
- Find intuitive documentation for matrixprofile-ts to help with understanding
- Coordinate with Sponsor about project proposal and literature review findings

#### Date:Feb 9, 2022
#### Time: 4pm-5pm

**Attendance:**

- Emani Hunter
- Will Diamond
- Mohammed Alyaqoub
- Ndeye Gueye

**Agenda:**

- Literature review discussion
  - Brief overview of findings and possible datasets/libraries that can be tested
  - Accumulate findings and possibly share with Romit (?)
- Proposal presentation slides
  - When to record presentation?
- Matrixprofile-ts (?)

**Discussion:**

**Literature Review:**

- Will:
  - Anomaly detection used to detect unknown attacks
  - Algorithms:
    - Supervised and unsupervised
      - Supervised more accurate, but requires labels
      - Unsupervised, faster, more broad
    - Idea: Support vector machine → dimensionality reduction, but not sure if it will work for data without labels
- Emani:
  - Unsupervised anomaly detection in time series data
  - 10 different benchmark datasets used
  - Idea: Apply CNN for unlabeled data
- Ndeye:
  - Fuzzy C-means clustering for time series data
  - Categorized anomalies in amplitude and anomalies in shape
  - Algorithms:
    - 1-NN Approach
    - Wavelet space partitioning
    - Sequential discounting autoregressive learning
    - Fuzzy C-means
      - Density based clustering techniques
      - Low-dimensional data
- Mohammed:
  - Tslearn → helps analyze time series
    - Time series clustering
  - STUMPY → time series mining
    - Similar to matrixprofle-ts

**Action Items / Next steps:**

- Each look at matrixprofile-ts repository and follow tutorial
  - Test example codes on 1-D data
- Test out algorithms found in papers
- Meet Saturday 6pm
  - Complete proposal presentation


#### Date:Jan 26, 2022
#### Time: 4pm-5pm


**Attendance:**

- Emani Hunter
- Will Diamond
- Mohammed Alyaqoub
- Ndeye Gueye


**Romit Background:**

- Computational/ Math and Comp Sci division
- US Department of Energy
- Argonne devoted to classical problems

**Project:**

- **Action item** : Do a literature review of time-series algorithms→ Google Scholar
  - Some literature may incorporate links to data used
  - Tool: researchrabbit.ai
    - Computational graph of papers that reference each other
    - Look at paper references
  - Look for high dimensional data
  - **Idea:** Stock market data is good to use (anomaly is currently happening)
    - Look at 400 companies
    - Don't do prediction
    - Do: Is there a catastrophic sell off I can predict?
  - Take notes during literature review/ annotate
  - _**Goal (not required):**_ Write a scientific journal
- **After literature review:** Romit can provide us benchmark data that he uses to apply these algorithms
  - Will send us equations/data
- **Issue:** datasets that anomaly detection community uses and datasets from dynamical systems team that Romit is apart of
  - These datasets can be encoded into a stream of data, but not sure if these anomaly detection methods can be applicable to this kind of data
  - Not a specific right answer, the biggest question is: _ **Can anomaly detection algorithms be applied to the dynamical systems data?** _
- **Important:** Computational efficiency, some methods may be slower in an optimal manner
- **Communication/ Meeting coordination:**
  - Slack channel
  - Email
  - Possible meetings