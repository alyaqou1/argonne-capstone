# Anomaly Detection in Time-Series Data
Ndeye Gueye, Emani Hunter, Mohammed Alyaqoub, Will Diamond


<div>
<img src="anomaly_detection.png" width="600"/>
</div>

### Introduction

This semester, our CMSE 495 team is working with Argonne National Laboratory as part of our capstone project to build an algorithm to assist with anomaly detection in time-series data that could be used throughout Argonne in its many different fields. The Argonne Capstone Team, consisting of MSU Data Science students Mohammed Alyaqoub, Will Diamond, Ndeye Gueye, and Emani Hunter, intends to complete a comprehensive study on the feasibility, speed, and accuracy of different anomaly detection algorithms for time series data using variable dimensions and sized data sets.

The Argonne Capstone Team’s goal is to answer two overarching questions, which later on will be broken down into smaller parts. Such questions consist of figuring out what solutions are available for time series anomaly detection and testing to see how well these solutions work. We will be working closely with Argonne to create an algorithm that will suit their needs while also gaining some valuable experience working with real-world data with a sponsor like Argonne.

Argonne National Laboratory is a science and engineering national research laboratory that conducts and provides research for the United States Department of Energy. Argonne’s extensive fields of research include physical science, environmental science, data science, and computational science. 


### Project Overview

The project will be broken down into four sections following the four-step pipeline: 

   - Collection/Generation of data:
        - Benchmark datasets found online 
        - Datasets provided by the sponsors 
        - Real-time dataset created by the team 


   - Cleaning/Munging: 
        - Preprocessing will be used to identify and clean the most common source of datasets 


   - Modeling building/Analysis:
        - We will research and test different time series anomaly detection algorithms
        - Sponsor provided sample datasets and matrixprofile-ts algorithm to experiment with for a start


<div>
<img src="matrixprofilets.png" width="600"/>
</div>

<div align="center">Simple Visualization of the starter dataset provided with matrixprofile-ts</div>


   - Visualization/Storytelling: 
        - We must demonstrate the feasibility of using the algorithms for this application, which includes reporting where the approaches work best and when they are likely to fail 


### Plans and Future

The Argonne Capstone team plans to start the process of developing a time-series anomaly detection algorithm after completing a literature review regarding anomaly detection in high-dimensional datasets and a period of research into Python libraries and algorithms that we believe will be helpful in the next stages of our project. We aim to get a starting algorithm that will accurately detect anomalies in low-dimensional datasets in the coming weeks before beginning the process of implementing that algorithm in a way that works on high-dimensional datasets in appropriate run-times that suit Argonne’s needs and plans.

We would like to thank Argonne for providing our CMSE 495 Capstone Team here at MSU for providing us with this opportunity to work in a real-world setting on this very interesting project. We would also like to thank Argonne’s representative, Dr. Romit Maulik, who has been very helpful to our team in our efforts to create an algorithm that will both help Argonne in its many research fields and help us gain some valuable knowledge and experience.



